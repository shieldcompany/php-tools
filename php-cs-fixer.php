<?php

$finder = PhpCsFixer\Finder::create()
    ->files()
    ->in(__DIR__)
    ->exclude('database')
    ->exclude('node_modules')
    ->exclude('storage')
    ->exclude('vendor')
    ->notName("*.txt")
    ->notName("*.code_workspace")
    ->ignoreDotFiles(true)
    ->ignoreVCS(true)
    ->ignoreVCSIgnored(true);

$config = new PhpCsFixer\Config();
return $config->setRules([
    // Arrays
    'array_syntax'                                => ['syntax' => 'short'],
    'no_multiline_whitespace_around_double_arrow' => true,
    'no_trailing_comma_in_singleline_array'       => true,
    'no_whitespace_before_comma_in_array'         => true,
    'normalize_index_brace'                       => true,
    'trim_array_spaces'                           => false,
    'whitespace_after_comma_in_array'             => true,
    // Basic
    'braces'   => [
        'allow_single_line_anonymous_class_with_empty_body' => true,
        'allow_single_line_closure'                         => false,
        'position_after_functions_and_oop_constructs'       => 'next',
        'position_after_control_structures'                 => 'next',
        'position_after_anonymous_constructs'               => 'next',
    ],
    'encoding' => true,
    // Casing
    'constant_case'                           => ['case' => 'lower'],
    'integer_literal_case'                    => true,
    'lowercase_keywords'                      => true,
    'lowercase_static_reference'              => true,
    'magic_constant_casing'                   => true,
    'magic_method_casing'                     => true,
    'native_function_casing'                  => true,
    'native_function_type_declaration_casing' => true,
    // Cast notation
    'cast_spaces'       => true,
    'lowercase_cast'    => true,
    'no_unset_cast'     => true,
    'short_scalar_cast' => true,
    // Class notation
    'class_attributes_separation' => [
        'elements' => ['const' => 'one', 'method' => 'one', 'property' => 'one', 'trait_import' => 'none']
    ],
    'class_definition'                   => ['single_line' => true, 'space_before_parenthesis' => false],
    'no_blank_lines_after_class_opening' => true,
    'no_null_property_initialization'    => true,
    'ordered_class_elements'             => ['order' => ['use_trait', 'property', 'constant', 'public', 'protected', 'private']],
    'protected_to_private'               => false,
    'single_class_element_per_statement' => ['elements' => ['const', 'property']],
    'single_trait_insert_per_statement'  => false,
    'visibility_required'                => ['elements' => ['property', 'method']],
    // Control structure
    'control_structure_continuation_position' => ['position' => 'next_line'],
    'elseif'                                  => false,
    'no_superfluous_elseif'                   => true,
    'no_trailing_comma_in_list_call'          => true,
    'no_unneeded_curly_braces'                => ['namespaces' => true],
    'no_useless_else'                         => true,
    'switch_case_semicolon_to_colon'          => true,
    'switch_case_space'                       => true,
    'switch_continue_to_break'                => false,
    // Function notation
    'function_declaration'          => ['closure_function_spacing' => 'none'],
    'function_typehint_space'       => true,
    'lambda_not_used_import'        => true,
    'method_argument_space'         => ['on_multiline' => 'ensure_fully_multiline', 'keep_multiple_spaces_after_comma' => false],
    'no_spaces_after_function_name' => true,
    'return_type_declaration'       => ['space_before' => 'one'],
    // Import
    'fully_qualified_strict_types' => true,
    'global_namespace_import'      => ['import_classes' => true, 'import_constants' => true, 'import_functions' => true],
    'no_leading_import_slash'      => true,
    'no_unused_imports'            => true,
    'single_import_per_statement'  => true,
    'single_line_after_imports'    => true,
    // Language construct
    'combine_consecutive_issets'   => true,
    'combine_consecutive_unsets'   => true,
    'declare_equal_normalize'      => ['space' => 'single'],
    'declare_parentheses'          => false,
    'explicit_indirect_variable'   => true,
    'single_space_after_construct' => [
        'constructs' => [
            'abstract', 'as', 'attribute', 'break', 'case', 'catch', 'class', 'clone', 'comment', 'const',
            'const_import', 'continue', 'echo', 'enum', 'extends', 'final', 'finally', 'function', 'function_import',
            'global', 'goto', 'implements', 'instanceof', 'insteadof', 'interface', 'match', 'named_argument',
            'namespace', 'new', 'php_doc', 'private', 'protected', 'public', 'readonly', 'return',
            'static', 'throw', 'trait', 'use', 'use_lambda', 'use_trait', 'var', 'yield', 'yield_from'
        ]
    ],
    // List notation
    'list_syntax' => ['syntax' => 'long'],
    // Namespace notation
    'blank_line_after_namespace'         => true,
    'clean_namespace'                    => true,
    'no_blank_lines_before_namespace'    => false,
    'no_leading_namespace_whitespace'    => true,
    'single_blank_line_before_namespace' => true,
    // Operators
    'assign_null_coalescing_to_coalesce_equal' => true,
    'binary_operator_spaces'                   => ['default' => 'align_single_space'],
    'concat_space'                             => ['spacing' => 'one'],
    'new_with_braces'                          => true,
    'no_space_around_double_colon'             => true,
    'not_operator_with_space'                  => true,
    'not_operator_with_successor_space'        => true,
    'object_operator_without_whitespace'       => true,
    'standardize_not_equals'                   => true,
    'ternary_operator_spaces'                  => true,
    'ternary_to_null_coalescing'               => true,
    'unary_operator_spaces'                    => true,
    // PHP tag
    'blank_line_after_opening_tag' => true,
    'echo_tag_syntax'              => ['format' => 'long'],
    'full_opening_tag'             => true,
    'linebreak_after_opening_tag'  => true,
    'no_closing_tag'               => true,
    // Return notation
    'no_useless_return'      => true,
    'return_assignment'      => false,
    'simplified_null_return' => true,
    // Semicolon
    'multiline_whitespace_before_semicolons'     => ['strategy' => 'no_multi_line'],
    'no_empty_statement'                         => true,
    'no_singleline_whitespace_before_semicolons' => true,
    'semicolon_after_instruction'                => true,
    'space_after_semicolon'                      => ['remove_in_empty_for_expressions' => true],
    // String
    'explicit_string_variable' => true,
    // Whitespace
    'array_indentation'            => true,
    'compact_nullable_typehint'    => true,
    'indentation_type'             => true,
    'method_chaining_indentation'  => true,
    'no_spaces_around_offset'      => ['positions' => ['outside']],
    'no_spaces_inside_parenthesis' => false,
    'no_trailing_whitespace'       => true,
    'no_whitespace_in_blank_line'  => true,
    'single_blank_line_at_eof'     => true,
    'types_spaces'                 => ['space' => 'single'],
])
    ->setFinder($finder);
