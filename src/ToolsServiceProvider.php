<?php

namespace ShieldCompany\Tools;

use Illuminate\Support\ServiceProvider;

class ToolsServiceProvider extends ServiceProvider
{
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        if ( $this->app->runningInConsole() )
        {
            $this->registerConsoleCommands();
        }
    }

    private function registerConsoleCommands()
    {
        $this->commands( Commands\SetupToolsCommand::class );
        $this->commands( Commands\FixCommand::class );
    }
}
