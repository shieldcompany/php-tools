<?php

namespace ShieldCompany\Tools\Commands;

use Illuminate\Console\Command;

class SetupToolsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'shield:grum';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets up the Shield Company dev tools (GrumPHP)';

    /**
     * The application path
     *
     * @var string
     */
    protected $path;

    public function __construct()
    {
        parent::__construct();

        $this->path = base_path();
    }

    public function handle()
    {
        $this->line( '-------------------------------------------------' );
        $this->info( 'Copying GrumPHP config file...' );
        $this->copyGrumConfigFile();

        $this->line( '-------------------------------------------------' );
        $this->info( 'Copying PHP-CS-Fixer config file...' );
        $this->copyPhpcsConfigFile();

        $this->line( '-------------------------------------------------' );
        $this->info( 'Initializes GrumPHP...' );
        $this->initGrum();

        $this->line( '-------------------------------------------------' );
        $this->info( 'Thanks for using Shield Company dev tools! ;) ' );
        $this->line( '-------------------------------------------------' );
    }

    private function copyGrumConfigFile()
    {
        $filename = 'grumphp.yml';
        $source   = 'vendor/shieldcompany/php-tools/' . $filename;
        $dest     = $this->path . '/' . $filename;

        $success = $this->copyFile( $source, $dest, 'GrumPHP config file' );
    }

    private function copyPhpcsConfigFile()
    {
        $filename = 'php-cs-fixer.php';
        $source   = 'vendor/shieldcompany/php-tools/' . $filename;
        $dest     = $this->path . '/' . $filename;

        $success = $this->copyFile( $source, $dest, 'PHP-CS-Fixer config file' );
    }

    private function copyFile($source, $destination, $name_for_feedback)
    {
        $folder = dirname($destination);

        if ( ! is_dir( $folder ) )
        {
            mkdir( dirname($destination), 0777, true );
        }

        if ( ! @copy( $source, $destination ) )
        {
            $errors = error_get_last();

            $this->error( 'An error occurred while copying ' . $name_for_feedback . ': ' . $errors['message'] );
            $this->error( 'You need to manually copy this file to the root of your project!' );

            return false;
        }


        $this->line( $name_for_feedback . ' successefuly copied' );

        return true;
    }

    private function initGrum()
    {
        $command = "php vendor/phpro/grumphp/bin/grumphp git:init";
        $output  = "";

        exec( "cd {$this->path} && {$command} 2>&1", $output );

        $this->line( $output );
    }
}
