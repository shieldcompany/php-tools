<?php

namespace ShieldCompany\Tools\Commands;

use Illuminate\Console\Command;

class FixCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'shield:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic fix Coding Standards issues';

    /**
     * The application path
     *
     * @var string
     */
    protected $path;

    public function __construct()
    {
        parent::__construct();

        $this->path = base_path();
    }

    public function handle()
    {
        $this->line( '-------------------------------------------------' );
        $this->info( 'Please wait, loading rules...' );
        $this->loadChanges();

        $this->line( '-------------------------------------------------' );
        $fix = $this->confirm( 'Execute changes? ' );

        if ( $fix )
        {
            $this->doFixes();
        }

        $this->line( '-------------------------------------------------' );
        $this->info( 'Thanks for using Shield Company dev tools! ;) ' );
        $this->line( '-------------------------------------------------' );
    }

    private function loadChanges()
    {
        $command = $this->getCommand( 'vendor/bin/php-cs-fixer' );
        $args    = [
            "--allow-risky=no",
            "--config=php-cs-fixer.php",
            "--using-cache=yes",
            "--path-mode=intersection",
            "--verbose",
            "--diff",
            "--dry-run",
            "."
        ];

        $full_command = $command . " " . implode(" ", $args);

        $this->executeShellCommand( $full_command );
    }

    private function doFixes()
    {
        $command = $this->getCommand( 'vendor/bin/php-cs-fixer' );
        $args    = [
            "--allow-risky=no",
            "--config=php-cs-fixer.php",
            "--using-cache=yes",
            "--path-mode=intersection",
            "--verbose",
            "fix",
            "."
        ];

        $full_command = $command . " " . implode(" ", $args);

        $this->executeShellCommand( $full_command );
    }

    private function getCommand($path)
    {
        $os = $this->currentOS();

        if ( $os == 'Windows' )
        {
            return $path . '.bat';
        }

        return $path;
    }

    /**
     * Returns current Operation System
     *
     * @return string
     */
    private function currentOS()
    {
        $uname = php_uname('s');

        if ( substr($uname, 0, 3) == 'Win' )
        {
            return 'Windows';
        }

        return 'Unix';
    }

    /**
     * Execute Shell command
     *
     * @param string $command
     * @return void
     */
    private function executeShellCommand($command)
    {
        $com = "cd " . $this->path . " && " . $command . " 2>&1";

        exec( $com );
    }
}
